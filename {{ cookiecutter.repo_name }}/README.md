{{cookiecutter.project_name}}
==============================

{{cookiecutter.description}}

## Project Organization

    ├── Makefile             <- Just write the command 'make' to see the help
    ├── README.md
    ├── data
    │   ├── processed        <- Datasets after being processed
    │   └── raw              <- Original datasets
    │
    ├── docs                 <- Scripts documentation
    │
    ├── models               <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks            <- Jupyter notebooks
    │
    ├── references           <- Papers or other documents if required
    │
    ├── reports
    │   └── figures
    │
    ├── requirements.txt     <- The requirements file for production
    ├── requirements-dev.txt <- The requirements file for dev env
    │
    ├── setup.py             <- makes project pip installable (pip install -e .) so src can be imported
    ├── src                  <- Source code for use in this project.
    │   ├── __init__.py      <- Makes src a Python module
    │   │
    │   ├── data             <- Scripts to download and convert data. Apply transformations if necessary
    │   │   └── make_dataset.py
    │   │
    │   ├── models         <- Scripts to train models and then use trained models to make
    │   │   │                 predictions
    │   │   ├── predict_model.py
    │   │   └── train_model.py

## Author(s)

## Technologies

## Install
Create the data folders and the virtual environment.  
In the root directory of this project:
```bash
make create_environment
```
Then activate the virtual environment newly created:
```bash
conda activate {{cookiecutter.description}} # If you use Anaconda Distribution
workon {{cookiecutter.description}} # Otherwise
```
And finally:
```bash
make install
```
This will install all libraries required to run the project.

## Datasets
### Dataset #1
#### Description
* Url: 
* Format:
* Resolution of images:
* Training data size:
* Test data size:

#### Download
In the root directory of this project:
```bash
make ...
```
This will download the training and test set in `data/raw/dataset_#1` (`data/raw` has to exist before, this is done with the command `make create_environment`). You should obtain:

    ├── dataset_#1
    │   ├── ...             <- Test set
    │   └── ...             <- train set

## Training
### Model #1

## Inference
### Model #1

## Internal References

## External References

## Known Bugs
